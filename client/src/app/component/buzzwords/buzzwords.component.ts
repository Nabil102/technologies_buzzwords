import { Component } from '@angular/core';
import { constants } from 'src/app/constants';
import { BuswordsServiceService } from '../../services/buswords-service.service';

@Component({
  selector: 'app-buzzwords',
  templateUrl: './buzzwords.component.html',
  styleUrls: ['./buzzwords.component.scss'],
})
export class BuzzwordsComponent {
  showBuzzword: boolean = false;
  technologyName: string = '';
  technologies: any;
  buzzwords: string | undefined;

  constructor(private technologyBuzzwords: BuswordsServiceService) {}

  ngOnInit() {
    this.technologyBuzzwords
      .getTechnologyBuzword()
      .subscribe((resTechnologies) => {
        this.technologies = resTechnologies;
      });
  }

  generateBuzzword() {
    this.showBuzzword = true;
    if(this.technologyName) {
      for (let i = 0; i < this.technologies.length; i++) {
        if (this.technologyName === constants.TECHNOLOGIE_ANGULAR) {
          this.buzzwords = this.technologies[i].angular.name;
        }
        if (this.technologyName === constants.TECHNOLOGIE_CLOUD) {
          this.buzzwords = this.technologies[i].clould.name;
        }
        if (this.technologyName === constants.TECHNOLOGIE_CSS) {
          this.buzzwords = this.technologies[i].css.name;
        }
        if (this.technologyName === constants.TECHNOLOGIE_DENO) {
          this.buzzwords = this.technologies[i].deno.name;
        }
        if (this.technologyName === constants.TECHNOLOGIE_ELIXIR) {
          this.buzzwords = this.technologies[i].elixir.name;
        }
        if (this.technologyName === constants.TECHNOLOGIE_FULTER) {
          this.buzzwords = this.technologies[i].fulter.name;
        }
        if (this.technologyName === constants.TECHNOLOGIE_GITLAB) {
          this.buzzwords = this.technologies[i].gitlab.name;
        }
        if (this.technologyName === constants.TECHNOLOGIE_HERUKO) {
          this.buzzwords = this.technologies[i].heruko.name;
        }
        if (this.technologyName === constants.TECHNOLOGIE_HTML) {
          this.buzzwords = this.technologies[i].html.name;
        }
        if (this.technologyName === constants.TECHNOLOGIE_JAVASCRIPT) {
          this.buzzwords = this.technologies[i].javascript.name;
        }
        if (this.technologyName === constants.TECHNOLOGIE_MATLAB) {
          this.buzzwords = this.technologies[i].Matlab.name;
        }
        if (this.technologyName === constants.TECHNOLOGIE_MONGODB) {
          this.buzzwords = this.technologies[i].mongodb.name;
        }
        if (this.technologyName === constants.TECHNOLOGIE_NODEJS) {
          this.buzzwords = this.technologies[i].nodejs.name;
        }
        if (this.technologyName === constants.TECHNOLOGIE_POSTGRER) {
          this.buzzwords = this.technologies[i].postgrer.name;
        }
        if (this.technologyName === constants.TECHNOLOGIE_PYTHON) {
          this.buzzwords = this.technologies[i].python.name;
        }
        if (this.technologyName === constants.TECHNOLOGIE_RAIL) {
          this.buzzwords = this.technologies[i].rail.name;
        }
        if (this.technologyName === constants.TECHNOLOGIE_REACT) {
          this.buzzwords = this.technologies[i].react.name;
        }
        if (this.technologyName === constants.TECHNOLOGIE_RUBY) {
          this.buzzwords = this.technologies[i].ruby.name;
        }
        if (this.technologyName === constants.TECHNOLOGIE_SASS) {
          this.buzzwords = this.technologies[i].sass.name;
        }
        if (this.technologyName === constants.TECHNOLOGIE_SWIFT) {
          this.buzzwords = this.technologies[i].swift.name;
        }
        if (this.technologyName === constants.TECHNOLOGIE_TYPSCRIPT) {
          this.buzzwords = this.technologies[i].typscript.name;
        }
      }

    }
     else if (this.technologyName === ''){
      this.buzzwords = "Le champs est vide, entrer une technologie (ex : html, css ou rail...)";
    }
  }
}
