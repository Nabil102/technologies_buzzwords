# Title : TLMgo-Coding challenge - Technologies buzzwords 
## API endpoint to get a ninja name buzzwords for technologie 
- the endpoint expoxte at /ninjify port 5000
- The endpoint returns a JSON with a ninja name
## Client interface to generate a ninja name for a technologie at port 4200
- Responsive
- Mobile/Desktop compatibility
 # Exemple : 
 endpoint :  http://localhost:5000/ninjify?x=javascript

returns : {"name":"au cœur des langages utilisés par les développeurs web"}

endpoint :  http://localhost:5000/ninjify?x=python

returns : {"name":"General-purpose programming language designed for readability"}
