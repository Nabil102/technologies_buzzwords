let cors = require("cors");
import { buzzwordsTechnologies } from "./data";
import express from "express";
import bodyParser from "body-parser";
import { Constants } from "./constants";

const app = express();

app.use(cors());

app.use(bodyParser.json());

const constants = new Constants();

app.get("/ninjify", (req, res) => {
  
  let {x} = req.query;

  let technologiesNames; 
  let buzzword;

  if(x?.length){
     technologiesNames = x?.toString().split(','); ; 
  }else {
    throw console.error("there's no element"); 
  }

  try {
    for (let i = 0; i < buzzwordsTechnologies.length; i++) {
      for (const technologieName of technologiesNames) {
        switch(technologieName){
          case constants.TECHNOLOGIE_HTML : 
          buzzword = buzzwordsTechnologies[i].html;
          break; 
          case constants.TECHNOLOGIE_RAIL : 
          buzzword = buzzwordsTechnologies[i].rail;
          break;
          case constants.TECHNOLOGIE_SASS : 
          buzzword = buzzwordsTechnologies[i].sass;
          break; 
          case constants.TECHNOLOGIE_JAVASCRIPT : 
          buzzword = buzzwordsTechnologies[i].javascript;
          break; 
          case constants.TECHNOLOGIE_ANGULAR : 
          buzzword = buzzwordsTechnologies[i].angular;
          break; 
          case constants.TECHNOLOGIE_NODEJS : 
          buzzword = buzzwordsTechnologies[i].nodejs;
          break; 
          case constants.TECHNOLOGIE_DENO : 
          buzzword = buzzwordsTechnologies[i].deno;
          break; 
          case constants.TECHNOLOGIE_FULTER : 
          buzzword = buzzwordsTechnologies[i].fulter;
          break;
          case constants.TECHNOLOGIE_CLOUD : 
          buzzword = buzzwordsTechnologies[i].clould;
          break; 
          case constants.TECHNOLOGIE_HERUKO: 
          buzzword = buzzwordsTechnologies[i].heruko;
          break; 
          case constants.TECHNOLOGIE_POSTGRER: 
          buzzword = buzzwordsTechnologies[i].postgrer;
          break;  
          case constants.TECHNOLOGIE_MATLAB: 
          buzzword = buzzwordsTechnologies[i].Matlab;
          break;  
          case constants.TECHNOLOGIE_MONGODB: 
          buzzword = buzzwordsTechnologies[i].mongodb;
          break;  
          case constants.TECHNOLOGIE_ELIXIR: 
          buzzword = buzzwordsTechnologies[i].elixir;
          break; 
          case constants.TECHNOLOGIE_REACT: 
          buzzword = buzzwordsTechnologies[i].react;
          break; 
          case constants.TECHNOLOGIE_CSS: 
          buzzword = buzzwordsTechnologies[i].css;
          break;
        }
      }
    }
  }catch(error){
    throw console.log(error); 
  }
  

  res.json(buzzword);
});

app.get("/ninjify/allBuzzwordsTechnologies", (req, res) => {
  let allTechnologies = buzzwordsTechnologies; 
  res.json(allTechnologies); 
} )

app.listen(5000, () => console.log("server started"));


