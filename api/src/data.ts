export const buzzwordsTechnologies = [
  {
    html: {
      name: "Crimson Drop Shadow",
    },
    sass: {
      name: "Crimson Drop Shadow",
    },
    rail: {
      name: "Crimson Drop Shadow",
    },
    javascript: {
      name: "au cœur des langages utilisés par les développeurs web",
    },
    css: {
      name: "Les feuilles de style en cascade",
    },
    python: {
      name: "General-purpose programming language designed for readability",
    },
    gitlab: {
      name: "GitLab est un logiciel libre de forge basé sur git",
    },
    typscript: {
      name: "Impossible sounding projects that exist",
    },
    react: {
      name: "JavaScript library for building user interfaces",
    },
    angular: {
      name: "Awesome list of Angular seed repos, starters, boilerplates",
    },
    mongodb: {
      name: "NoSQL database",
    },
    postgrer: {
      name: "Object-relational database",
    },
    clould: {
      name: "The fastest way to get serverless",
    },
    Matlab: {
      name: "Designed for the way you think and the work you do",
    },
    heruko: {
      name: "Heroku est une entreprise créant des logiciels",
    },
    fulter: {
      name: "Expertise, quality, flexibility and diligence",
    },
    nodejs: {
      name: "Async non-blocking event-driven JavaScript runtime",
    },
    deno: {
      name: "A secure runtime for JavaScript and TypeScript that uses V8 and is built in Rust",
    },
    capacitor: {
      name: "Cross-platform open source runtime for building Web Native apps",
    },
    swift: {
      name: "Apple's compiled programming language that is secure",
    },
    elixir: {
      name: "A curated list of amazingly awesome Elixir libraries",
    },
    ruby: {
      name: "A categorized community-driven collection of awesome Ruby libraries",
    },
  },
];
