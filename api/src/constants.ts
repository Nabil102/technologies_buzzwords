export class Constants {
   public TECHNOLOGIE_HTML = 'html';
   public TECHNOLOGIE_SASS = 'sass';
   public TECHNOLOGIE_RAIL = 'rail';
   public TECHNOLOGIE_JAVASCRIPT = 'javascript';
   public TECHNOLOGIE_CSS = 'css';
   public TECHNOLOGIE_PYTHON = 'python';
   public TECHNOLOGIE_GITLAB = 'gitlab';
   public TECHNOLOGIE_TYPSCRIPT= 'typscript';
   public TECHNOLOGIE_REACT = 'react';
   public TECHNOLOGIE_ANGULAR = 'angular';
   public TECHNOLOGIE_MONGODB = 'mongodb';
   public TECHNOLOGIE_POSTGRER = 'postgrer';
   public TECHNOLOGIE_CLOUD = 'clould';
   public TECHNOLOGIE_MATLAB = 'Matlab';
   public TECHNOLOGIE_HERUKO = 'heruko';
   public TECHNOLOGIE_FULTER = 'fulter';
   public TECHNOLOGIE_NODEJS = 'nodjs';
   public TECHNOLOGIE_DENO= 'deno';
   public TECHNOLOGIE_SWIFT = 'swift';
   public TECHNOLOGIE_ELIXIR = 'elixir';
   public TECHNOLOGIE_RUBY = 'ruby';
  }
  
  export const constants = new Constants();
  